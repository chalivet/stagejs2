'use strict';

/**
 * 
 * @param {*} adressServeur 'http://localhost:7500'
 * 
 * objet CRUD pour server REST
 */
function RestCRUD(adressServeur) {
    if (undefined == adressServeur) return null;

    var _VM = this; // stockage contexte d'origine
    var _REST_ADR = adressServeur; //privée
    this.lastResult = null;

    /**
     * read == GET sur le server
     */
    this.read = function(ressource, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', _REST_ADR + ressource, true);
        xhr.onreadystatechange = function(evt) {
            var _xhr = evt.target;
            if (_xhr.readyState < 4 || _xhr.status < 200 || _xhr.status >= 300) return;
            console.log(_xhr.response);
            this.lastResult = xhr.responseText;
            if (undefined != callback) callback(_xhr.response);
        };
        xhr.send();
    };

    /**
     * create == POST sur le server
     */
    this.create = function(objectRessource, ressource, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', _REST_ADR + ressource, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function(evt) {
            var _xhr = evt.target;
            if (_xhr.readyState < 4 || _xhr.status < 200 || _xhr.status >= 300) return;
            console.log(_xhr.response);
            this.lastResult = xhr.responseText;
            if (undefined != callback) callback(_xhr.response);
        };
        xhr.send(JSON.stringify(objectRessource));
    };

    /**
     * update == PUT sur le server
     */
    this.update = function(objectRessource, ressource, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('PUT', _REST_ADR + ressource, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function(evt) {
            var _xhr = evt.target;
            if (_xhr.readyState < 4 || _xhr.status < 200 || _xhr.status >= 300) return;
            console.log(_xhr.response);
            this.lastResult = xhr.responseText;
            if (undefined != callback) callback(_xhr.response);
        };
        xhr.send(JSON.stringify(objectRessource));
    };

    /**
     * delete == DELETE sur le server
     */
    this.delete = function(ressource, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('DELETE', _REST_ADR + ressource, true);
        xhr.onreadystatechange = function(evt) {
            var _xhr = evt.target;
            if (_xhr.readyState < 4 || _xhr.status < 200 || _xhr.status >= 300) return;
            console.log(_xhr.response);
            if (undefined != callback) callback(_xhr.response);
        };
        xhr.send();
    };
}