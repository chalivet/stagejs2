'use strict';
/**
 * bool pour toggleeditform ( var globale )
 */
var isformShown = true;

var notes_restcrud = new RestCRUD('http://localhost:7500'); // new : constructeur objet

var mymap;

jsLoaded();

function jsLoaded() {
    var jsloaded = document.querySelector('#is-js-loading');
    jsloaded.style.display = 'none';
    console.log(jsloaded);
    initFormEditNote();
}

function initNoteContainer() {

}

function initFormEditNote() {
    var a = document.forms["form-edit-note"];
    //a.onsubmit = submitnote; equivalent mais hasbeen
    a.addEventListener('submit', submitnote);
    a.addEventListener('reset', resetnote);
    document.forms["form-edit-note"].querySelectorAll('input:not(.btn), textarea, select').forEach(element => {
        element.addEventListener('focus', function(evt) {
            console.log(evt);
            evt.target.classList.remove('invalid-input');
        });
    });
    document.getElementById('toggle-form-edit-note').addEventListener('click', toggleeditform);

    toggleeditform(null); // repli du formulaire

    mymap = L.map('mapid').setView([51.505, -0.09], 13);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);
}

/**
 * 
 * @param {*} evt 
 * repli du formulaire
 */
function toggleeditform(evt) {
    isformShown = !isformShown;
    document.querySelector('.form-group').style.display = (isformShown) ? 'block' : 'none';
}

function submitnote(evt) { // en minuscules pour addEventListener
    //if (undefined === evt) return;
    evt.preventDefault();
    console.log(evt);

    var objNewNote = checkValidityOfForm();
    if (objNewNote == null) return;
    var img = objNewNote.userimg;
    delete(objNewNote.userimg);
    var name = objNewNote.username;
    delete(objNewNote.username);
    notes_restcrud.create(objNewNote, '/notes', function(response) {
        var noteComplete = JSON.parse(response);
        noteComplete.userimg = img;
        noteComplete.username = name;
        addNoteToList(noteComplete); // attente du retour du server, une fois la note enregistrée et renvoyée avec id
        document.forms["form-edit-note"].reset();
    });

}

function resetnote(evt) {
    evt.preventDefault();
    //var a = document.forms["form-edit-note"].reset();
    var nodes = document.forms["form-edit-note"].querySelectorAll('input:not(.btn), textarea');
    nodes.forEach(function(element) { element.value = "" });

    nodes = document.forms["form-edit-note"].querySelectorAll('input:not(.btn), textarea, select').forEach(function(element) { element.classList.remove('invalid-input') });

    document.forms["form-edit-note"]['input-user'].selectedIndex = -1;
}

function checkValidityOfForm() {
    var valid = true;

    var titreOfNote = document.forms['form-edit-note']['input-titre'];
    if (titreOfNote.value.length <= 2) {
        valid = false;
        titreOfNote.classList.add('invalid-input');
    } else {
        titreOfNote.classList.remove('invalid-input');
    }

    var date = document.forms['form-edit-note']['input-date'];
    // tests pour navigateur HTML5 ou pas (type Date ou chaine)
    if (undefined == date.value || (String(date.value).length != 10 || Date.parse(date.value) == undefined)) {
        valid = false;
        date.classList.add('invalid-input');
    } else {
        date.classList.remove('invalid-input');
    }

    var time = document.forms['form-edit-note']['input-time'];
    const regex = /^([01]\d|2[0-3]):[0-5]\d$/gm; //regex101.com
    if (regex.exec(time.value) == null) {
        valid = false;
        time.classList.add('invalid-input');
    } else {
        time.classList.remove('invalid-input');
    }

    var description = document.forms['form-edit-note']['input-description'];
    if (description.value == "") {
        valid = false;
        description.classList.add('invalid-input');
    } else {
        description.classList.remove('invalid-input');
    }

    var user = document.forms['form-edit-note']['input-user'];
    var userName = '';
    var userid = '';
    if (-1 == user.selectedIndex || user.value == '') {
        valid = false;
        user.classList.add('invalid-input');
    } else {
        user.classList.remove('invalid-input');
        var infosUser = user.options[user.selectedIndex].innerHTML.split(':');
        userid = infosUser[0];
        userName = infosUser[1];
    }

    console.log(titreOfNote, date, time, user, userName, description);
    //notes_restcrud.create(maDivNote, '/notes', null);

    return (valid) ? {
        titre: titreOfNote.value,
        date: date.value,
        time: time.value,
        description: description.value,
        userimg: user.value,
        user: userid,
        username: userName
    } : null;
}

function addNoteToList(objNoteToAdd) {
    if (undefined === objNoteToAdd && ((objNoteToAdd = checkValidityOfForm()) == null)) return false;

    console.log(objNoteToAdd);

    var maDivNote = document.createElement('div');
    maDivNote.classList.add('note');
    maDivNote.id = 'note-' + objNoteToAdd.id;
    maDivNote.innerHTML = '<div class="img-close"><img src="img/close.png" /></div><div class="note-user"><img src="img/' + objNoteToAdd.userimg + '"><br>' + objNoteToAdd.user + ':' + objNoteToAdd.username + '</div><div class="note-right"><div class="note-titre">' + objNoteToAdd.titre + '</div><div class="note-datetime">' + objNoteToAdd.date + ' à ' + objNoteToAdd.time + '</div></div><div class="note-description">' + objNoteToAdd.description + '</div>';

    maDivNote.querySelector('.img-close img').addEventListener('dblclick', deletenote);
    document.querySelector('#notes-container').appendChild(maDivNote);

    return true;
}

function deletenote(evt) {
    console.log(evt.target);
    var isOkToDelete = confirm('Souhaitez vous vrament supprimer cette note ?');
    if (isOkToDelete) {
        var idnote = evt.target.parentElement.parentElement.id.replace('note-', '');
        notes_restcrud.delete('/notes/' + idnote, function() {
            alert('suppression ok');
            evt.target.parentElement.parentElement.remove(); // img puis div puis note
        });
    }
}


notes_restcrud.read('/notes', function(responseText) {
    var myParseOfJson = JSON.parse(responseText);
    //console.log(myParseOfJson);
    myParseOfJson.map(elem => {
        notes_restcrud.read('/users/' + elem.user, function(resp) {
            var userObjResponse = JSON.parse(resp);
            elem.user = elem.user;
            elem.userimg = userObjResponse.img;
            elem.username = userObjResponse.name;
            addNoteToList(elem);
        });

    });

});